﻿using Microsoft.AspNetCore.Mvc;
using Models.Admin.AdminLoginModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEndProject.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminLoginController : ControllerBase
    {
        [HttpPost]
        public ActionResult Post([FromBody] AdminLoginRequestModel login)
        {
            return Ok(login);
        }
    }
}
