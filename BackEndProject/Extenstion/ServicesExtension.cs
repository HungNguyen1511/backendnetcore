﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Extension
{
    public static class ServicesExtension
    {
        public static void AddSwagger(this IServiceCollection service)
        {
            service.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });
        }

    }
}
