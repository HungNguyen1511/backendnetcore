﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    public class Cart : BaseEntity
    {
        public int ProductId { set; get; }
        public int Quantity { set; get; }
        public decimal Price { set; get; }
    }
}
