﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("Customer")]
    public class Customer : BaseEntity
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Address { set; get; }
        public string City { set; get; }
        public string Country { set; get; }
        public string Phone { set; get; }
    }
}
