﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("Employee")]
    public class Employee : BaseEntity
    {
        public string LastName { set; get; }
        public string FirstName { set; get; }
        public DateTime DateOfBirth { set; get; }
        public DateTime DateHire { set; get; }

        public string Address { set; get; }
        public string City { set; get; }
        public string Country { set; get; }
        public string Region { set; get; }
        public string PhoneNumber { set; get; }
        public string Photo { set; get; }

    }
}
