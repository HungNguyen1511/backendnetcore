﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("Order")]
    public class Order : BaseEntity
    {
        public int CustomerID { set; get; }
        public int EmployeeID { set; get; }
        public DateTime OrderDate { set; get; }
        public DateTime ShipDate { set; get; }
        public string ShipName { set; get; }
        public string ShipAddress { set; get; }
    }
}
