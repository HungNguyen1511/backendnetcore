﻿using DAL.DataModel.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.DataModel.BusinessModel
{
    [Serializable]
    [Table("OrderDetail")]
    public class OrderDetail : BaseEntity
    {
        public int ProductID { set; get; }
        public int OrderID { set; get; }
        public double UnitPrice { set; get; }
        public int Quantity { set; get; }
        public double Discount { set; get;}
    }
}
