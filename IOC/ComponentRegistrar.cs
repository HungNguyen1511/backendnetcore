﻿using DAL.DataModel.BusinessModel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Services.Implementation;
using Services.Interface;

namespace IoC
{
    public static class ComponentRegistrar
    {
        public static void InitComponent(this IServiceCollection services)
        {
            ////Context
            //services.AddScoped(typeof(CoffeeRenoContext), typeof(CoffeeRenoContext));
            //services.AddScoped(typeof(ApplicationDbContext), typeof(ApplicationDbContext));
            services.AddScoped(typeof(IRepository<Category>), typeof(CategoryRepository));
            //// Authentication
            //services.AddScoped(typeof(JwtIssuerOptions), typeof(JwtIssuerOptions));
            ////Repository
            //RegRepository(services);

            ////Service
            //RegServices(services);
            //services.AddTransient<IImageHandler, ImageHandler>();
            //services.AddTransient<IImageWriter, ImageWriter>();
            //services.AddTransient<IEmailService, EmailService>();
            ////Common
            //services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddSingleton<IJwtFactory, JwtFactory>();
            //services.AddScoped<IAuthService, AuthService>();

        }

        //private static void RegRepository(IServiceCollection services)
        //{
        //    services.AddScoped(typeof(IApplicationLanguageRepository), typeof(ApplicationLanguageRepository));
        //    services.AddScoped(typeof(ILocationRepository), typeof(LocationRepository));
        //}
        //private static void RegServices(IServiceCollection services)
        //{
        //    services.AddScoped(typeof(ICommonServices), typeof(CommonServices));
        //}
    }
}
