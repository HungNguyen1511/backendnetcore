﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Implementation;
using Services.Interface;


namespace IOC
{
    public class ServiceContainer : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            //builder.RegisterType<ShopContext>().As<ShopContext> ();
            builder.RegisterType<CategoryRepository>().As<IRepository<Category>>();
            //builder.RegisterType<APIExcution>().As<IAPIExcution>();
            //builder.RegisterType<DepositService>().As<IDepositService>();
        }
    }

}
