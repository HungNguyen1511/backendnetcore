﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Message.Logs;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using Models.Messaging;


namespace Message.Email
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailRequestModel model);
    }
    public class EmailService : IEmailService
    {
        private readonly EmailConfig _ec;

        public EmailService(IOptions<EmailConfig> emailConfig)
        {
            this._ec = emailConfig.Value;
        }

        public async Task SendEmailAsync(EmailRequestModel requestModel)
        {
            try
            {
                //var emailMessage = new MimeMessage();

                //emailMessage.From.Add(new MailboxAddress(_ec.FromName, _ec.FromAddress));
                //emailMessage.To.Add(new MailboxAddress("", requestModel.Email));
                //emailMessage.Subject = requestModel.Subject;
                //emailMessage.Body = new TextPart(TextFormat.Html) { Text = requestModel.Message };

                //using (var client = new SmtpClient())
                //{                   
                //    client.LocalDomain = _ec.LocalDomain;

                //    await client.ConnectAsync(_ec.MailServerAddress, Convert.ToInt32(_ec.MailServerPort)).ConfigureAwait(false);
                //    await client.AuthenticateAsync(new NetworkCredential(_ec.UserId, _ec.UserPassword));
                //    await client.SendAsync(emailMessage).ConfigureAwait(false);
                //    await client.DisconnectAsync(true).ConfigureAwait(false);
                //}
                //var message = new MailMessage("phonglinh260888999@gmail.com","hunghvhpu@gmail.com")
                // smtp.Credentials = new NetworkCredential("phonglinh260888999@gmail.com", "26081989aB@");

                //------------------------------------------------------------------------------------------
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(_ec.FromAddress);
                message.To.Add(new MailAddress(requestModel.Email));
                message.Subject = requestModel.Subject;
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = requestModel.Message;
                smtp.Port = 587;
                smtp.Host = _ec.MailServerAddress; //for gmail host  
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_ec.UserId, _ec.UserPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message + _ec.UserId + _ec.UserPassword);
                throw;
            }
        }
    }

}
