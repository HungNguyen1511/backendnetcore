﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Dapper;
using Shared.ServiceLocator;

namespace Message.Logs
{
    public static class Logger
    {
        public static void LogError(string message, string methodName = "", string ip = "", string browser = "")
        {
            try
            {
                var ctx = DapperConnection.Current.Connection;
                using (IDbConnection conn = ctx)
                {
                    string sQuery = "INSERT INTO Logs (Message, CreateDate, Ip,Browser,MethodName) VALUES(@Message, @CreateDate, @Ip, @Browser, @MethodName)";
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@Message", message);
                    parameters.Add("@CreateDate", DateTime.Now);
                    parameters.Add("@Ip", ip);
                    parameters.Add("@Browser", browser);
                    parameters.Add("@MethodName", methodName);
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();
                    conn.Execute(sQuery, parameters, commandType: CommandType.Text);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

    }
}
