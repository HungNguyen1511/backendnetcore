﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Admin.AdminProductModel
{
    public class RequestAddNewProductModel
    {
        public string product_name { set; get; }
        public int creators { get; set; }
    }
}
