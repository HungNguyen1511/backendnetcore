﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class ResponseMessageForList<T>
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public T Data { get; set; }
    }
    //Data Access Layer
    // IOC Inversion Of Container
    // Message Thông điệp
    // Model Model sử dụng cho business
    // Service Xử lý logic
    
    public class ResponeMessageForList<T>
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<T> Data { get; set; }
    }
}
