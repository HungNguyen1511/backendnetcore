﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class CartRepository
    {
        private ShopContext db;
        public CartRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Cart entity)
        {
            db.Carts.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedCategory = db.Carts.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedCategory == null)
            {
                return 0;
            }
            selectedCategory.IsDeleted = true;
            selectedCategory.DeletedDate = DateTime.Now;
            selectedCategory.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId, Cart entity, int userid)
        {
            var updateCart = db.Carts.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateCart == null)
            {
                // return BadRequest(); 
            }
            updateCart.ModifiedBy = userid;
            updateCart.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Cart GetById(int id)
        {
            return db.Carts.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Cart> List()
        {
            return db.Carts.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Cart> List(Expression<Func<Cart, bool>> predicate)
        {
            return db.Carts.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
