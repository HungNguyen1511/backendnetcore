﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class CustomerRepository : IRepository<Customer>
    {
        private ShopContext db;
        public CustomerRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Customer entity)
        {
            db.Customers.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedCustomer = db.Customers.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedCustomer == null)
            {
                return 0;
            }
            selectedCustomer.IsDeleted = true;
            selectedCustomer.DeletedDate = DateTime.Now;
            selectedCustomer.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId, Customer entity, int userid)
        {
            var updateCustomer = db.Customers.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateCustomer == null)
            {
                // return BadRequest(); 
            }
            updateCustomer.ModifiedBy = userid;
            updateCustomer.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Customer GetById(int id)
        {
            return db.Customers.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Customer> List()
        {
            return db.Customers.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Customer> List(Expression<Func<Customer, bool>> predicate)
        {
            return db.Customers.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
