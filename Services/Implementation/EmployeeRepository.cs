﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private ShopContext db;
        public EmployeeRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Employee entity)
        {
            db.Employees.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedEmployee = db.Employees.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedEmployee == null)
            {
                return 0;
            }
            selectedEmployee.IsDeleted = true;
            selectedEmployee.DeletedDate = DateTime.Now;
            selectedEmployee.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId, Employee entity, int userid)
        {
            var updateEmployee = db.Employees.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateEmployee == null)
            {
                // return BadRequest(); 
            }
            updateEmployee.ModifiedBy = userid;
            updateEmployee.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Employee GetById(int id)
        {
            return db.Employees.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Employee> List()
        {
            return db.Employees.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Employee> List(Expression<Func<Employee, bool>> predicate)
        {
            return db.Employees.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
