﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class OrderDetailRepository : IRepository<OrderDetail>
    {
        private ShopContext db;
        public OrderDetailRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(OrderDetail entity)
        {
            db.OrderDetails.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedOrderDetail = db.OrderDetails.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedOrderDetail == null)
            {
                return 0;
            }
            selectedOrderDetail.IsDeleted = true;
            selectedOrderDetail.DeletedDate = DateTime.Now;
            selectedOrderDetail.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId, OrderDetail entity, int userid)
        {
            var updateOrderDetail = db.OrderDetails.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateOrderDetail == null)
            {
                // return BadRequest(); 
            }
            updateOrderDetail.ModifiedBy = userid;
            updateOrderDetail.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public OrderDetail GetById(int id)
        {
            return db.OrderDetails.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<OrderDetail> List()
        {
            return db.OrderDetails.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<OrderDetail> List(Expression<Func<OrderDetail, bool>> predicate)
        {
            return db.OrderDetails.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
