﻿using DAL.DataModel.BusinessModel;
using DAL.DBContext;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class OrderRepository : IRepository<Order>
    {
        private ShopContext db;
        public OrderRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Order entity)
        {
            db.Orders.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedOrder = db.Orders.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedOrder == null)
            {
                return 0;
            }
            selectedOrder.IsDeleted = true;
            selectedOrder.DeletedDate = DateTime.Now;
            selectedOrder.DeletedBy = userid;
            db.SaveChanges();
            return 1;
        }

        public void Edit(int originalId, Order entity, int userid)
        {
            var selectedOrder = db.Orders.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (selectedOrder == null)
            {
                // return BadRequest(); 
            }
            selectedOrder.ModifiedBy = userid;
            selectedOrder.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Order GetById(int id)
        {
            return db.Orders.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Order> List()
        {
            return db.Orders.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Order> List(Expression<Func<Order, bool>> predicate)
        {
            return db.Orders.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
