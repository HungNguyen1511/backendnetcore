﻿using Services.Interface;
using DAL.DataModel.BusinessModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using DAL.DBContext;
using System.Linq;

namespace Services.Implementation
{
    public class ProductRepository : IRepository<Product>
    {
        private ShopContext db;
        public ProductRepository(ShopContext shopContext)
        {
            db = shopContext;
        }
        public void Add(Product entity)
        {
            db.Products.Add(entity);
            db.SaveChanges();
        }

        public int Delete(int id, int userid)
        {
            var selectedProduct = db.Products.Where(c => c.IsDeleted == false && c.Id == id).FirstOrDefault();
            if (selectedProduct == null)
            {
                return 0;
            }
            selectedProduct.IsDeleted = true;
            selectedProduct.DeletedDate = DateTime.Now;
            selectedProduct.DeletedBy = userid;
            db.SaveChanges();
            return 1;

        }

        public void Edit(int originalId, Product entity, int userid)
        {
            var updateProduct = db.Products.Where(x => x.Id == originalId && x.IsDeleted == false).FirstOrDefault();
            if (updateProduct == null)
            {
                // return BadRequest(); 
            }
            updateProduct.ProductName = entity.ProductName;
            updateProduct.ModifiedBy = userid;
            updateProduct.ModifiedDate = DateTime.Now;
            db.SaveChanges();
        }

        public Product GetById(int id)
        {
            return db.Products.Where(c => c.Id == id && c.IsDeleted == false).FirstOrDefault();
        }

        public IEnumerable<Product> List()
        {
            return db.Products.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<Product> List(Expression<Func<Product, bool>> predicate)
        {
            return db.Products.Where(predicate).Where(c => c.IsDeleted == false).ToList();
        }
    }
}
