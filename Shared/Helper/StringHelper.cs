﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Shared.Helper
{
    public static class StringHelper
    {
        static Regex _convertToUnsignRg = null;
        public static string ConvertToUnsign(this string strInput)
        {
            if (ReferenceEquals(_convertToUnsignRg, null))
            {
                _convertToUnsignRg = new Regex("p{IsCombiningDiacriticalMarks}+");
            }
            var temp = strInput.Normalize(NormalizationForm.FormD);
            return _convertToUnsignRg.Replace(temp, string.Empty).Replace("đ", "d").Replace("Đ", "D").ToLower();
        }
    }
}
