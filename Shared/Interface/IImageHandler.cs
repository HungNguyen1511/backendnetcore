﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Shared.Interface
{
    public interface IImageHandler
    {
        Task<string> UploadImage(IFormFile file);
        Task<bool> DeleteImage(string file);
        Task<byte[]> GetFile(string fileName);
    }

}
