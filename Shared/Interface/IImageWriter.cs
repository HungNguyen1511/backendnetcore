﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;


namespace Shared.Interface
{
    public interface IImageWriter
    {
        Task<string> UploadImage(IFormFile file);
        Task<bool> DeleteImage(string fileUrl);
        Task<byte[]> GetFile(string fileUrl);
    }
}
