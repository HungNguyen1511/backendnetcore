﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MySql.Data.MySqlClient; // cái này kết nối với MySql phải đổi thành kết nối với Sql Server như thế nào

namespace Shared.ServiceLocator
{
    public class DapperConnection
    {
        private readonly IConfiguration _config;
        private static IConfiguration _configProvider;
        public DapperConnection(IConfiguration config)
        {
            _config = config;
        }
        public IDbConnection Connection
        {
            get
            {
                return new MySqlConnection(_config.GetConnectionString("Coffee"));
            }
        }
        public static DapperConnection Current
        {
            get
            {
                return new DapperConnection(_configProvider);
            }
        }
        public static void SetLocatorProvider(IConfiguration config)
        {
            _configProvider = config;
        }

    }
}
