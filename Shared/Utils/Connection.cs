﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Utils
{
    public static class Connection
    {
        public enum DatabaseName
        {
            Coffee,
            Logging
        }
        public static MySqlConnection GetConnection(DatabaseName dbName, IConfiguration config)
        {
            return new MySqlConnection(GetConnectionString(dbName, config));
        }

        private static string GetConnectionString(DatabaseName dbName, IConfiguration config)
        {
            var connectionString = config.GetConnectionString(dbName.ToString());
            if (connectionString == null)
                throw new Exception($"Connection string {dbName} not in config");

            return connectionString;
        }

    }
}
